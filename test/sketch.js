var colorNum;
var x;
var y;
var esize;
var hueColor;

//Number of different colors you want (can be changed)
colorNum = 12;

function setup() {
  createCanvas(800,800);
  frameRate(30);
  colorMode(HSL);
}

function drawEllipse() {
  x = random(630);
  y = random(630);
  esize = random(500,700);

  hueColor = floor(random(colorNum));
  noStroke();
  fill(hueColor*360/colorNum,360,45);
  ellipse(x, y, esize);
}

function draw() {


  if (frameCount < 40) {
push()
  drawingContext.filter = 'blur(175px)';
  drawEllipse();

pop()
} else {
  textFont('Helvetica');
  textSize(100);
  textStyle(BOLD);
  fill(255);
  textAlign(CENTER);
  text('GRADIENT.', width/2, height/2);
  }
}

![](minix2-billede.png)


**<h1>Emoji-game</h1>**
___
<h5>Victor Rasmussen</h5>
<br>
<br>
<br>

<h5>Advert: This new Emoji-game makes you buy instagram-posts in order to reach 10k followers!</h5>
 <br>
<h5>Buy instagram posts and make emoji happy by reaching 10k followers! </h5>
<br>
<br>
<p>
Im not entirely sure what my program is. It’s been influenced by a lot of different things. First of all my whole idea started with the idea of emoji as human representation. In some way emojis are very similar and humanlike. But i also feel a difference, a gap, between humans and emoji, and my thought was to someway make a program that could bridge this in some way. Because human representation in emoji, never occured to me before, because i really haven't paid much attention to this idea. I thought it would be a good place for me to start.
<p/>

<p>
My program is a hybrid between an instagram profile and cookieclicker. Why i chose to make my emoji representation on an instagram imitation is a thought of making the emoji more human. Assigning an emoji a profile, makes it seem more real in some way. As we begin to attribute different things to emojis the reference to human begin. My emoji is coloured purple because its a color i think isn’t linked with skin colour. I’m in despair of a solution to skin-colouring of emojis, i think it’s a bad fix, but i’m at the same time not clever enough to produce a satisfaying solution. The reason i chose to incorperate a cookie clicker, afk game type, element is also to make the emoji seem more humanlike. The emoji smiles when you reach 10 thousand followers, and is sad when on 0. This attribution of feelings is an attempt to give the emoji human qualities.  At the same time instagram is a social media with two sides to it. Instagram can be superficial and hide the other side, and for me theres is something symbolic in the emoji being represented on a page, which in some way hide the full story. It’s hard to exactly explain why i chose to program this and in a way it reflects my thinking in a way, where i don’t think everybody would get it or maybe find it to abstract. It’s also in a way a comment on a conversation i had with my younger brother where he said the only emoji you use on social media that mean something real, is the heart emoji. The idea of not giving any other emoji a power in terms of representation, is a foreign thought to me now, and where the idea of incorperating Instagram begun.
<p/>

<p>

<h5>It’s important for me to say that this program is in no way meant to critisize social media, or the use of social media in any way.</h5>

<p/>


<p>
In terms of programming it’s been an insteresting journey. My focus for this time is to use functions in a flexible way. When for example i had to program the buttons, i wanted to make a function to call anytime i wanted to create a new button. This task was hard as i needed a way to store if a button had been pressed. This meant having different parameters in the function which could be changed if the button was pressed, or so i thought. I tried having a variable being changed, but a problem occured not being able to change the variable i set in the parameter of the function. A lot of hours later i found out that i could use <code>return</code> and give a boolean value as output for the whole function when within a if-statement. This enabled me to run checks to see if the buttons were being pressed.
<p/>


<p>
I learned how to preload images by following the references on <code>image()</code> in P5 references. I also made use of <code>curve()</code> which reminds me a lot of Photoshop. I also learned this from the P5 references.
<p/>

<p>
I also had to make use of math from high school. I had to check up on linear regression in order to make the mouth of the smiley react to the amount of followers.
<p/>


<h3> RUNME & REPOSITORY </h3>

[RUNME](https://vras.gitlab.io/aestheticprogramming/minix2/)
<br>
[My repository](https://gitlab.com/VRAS/aestheticprogramming)




<h3> Reference </h3>

[p5.js](https://p5js.org/)
<br>

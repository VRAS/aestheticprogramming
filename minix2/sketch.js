
//preload

let img1;
let img2;
let img3;
let img4;

let showImg1 = 0;
let showImg2 = 0;
let showImg3 = 0;
let showImg4 = 0;




//Preloading images
//take link from files on website to pass in (saves you a lot of time)
function preload() {
  img1 = loadImage('01.jpg');
  img2 = loadImage('02.jpg');
  img4 = loadImage('03.jpg');
  img3 = loadImage('04.jpg');
}
//Værdier for at lave knapper og registrere klik og køb
let lamboPrize = 150;
let yachtPrize = 1000;
let mansionPrize = 2000;
let rolexPrize = 30;


let interval = 2;

let followspeed = 1;

let follower;

let chosenColour1 = 100;
let chosenColour2 = 100;
let chosenColour3 = 100;
let chosenColour4 = 100;



follower = 1;

lambo = "Lambo";
mansion = "Mansion";
rolex = "Rolex";
yacht = "Yacht";

let colorOutput1 = 100;
let colorOutputDone1 = [0,0, colorOutput1];


//Størrelse på firkanter på knapper
let rectWidth = 100;
let rectSize = 40;

function setup() {
  createCanvas(800,800);
  background(300,100,70);
  frameRate(60);
  colorMode(HSL);
}




// En form får hitbox detection
function trueClick(mouseX, mouseY, rectX, rectY) {


//Hvis mouseX, mouseY er indefor targetarray1 og targetarray2 og mouseclick er trueClick --> farveændring & buy1
let x2 =  rectX + rectWidth;
let y2 = rectY + rectSize;

if ((mouseIsPressed == true) && (mouseX > rectX && mouseX < x2) && (mouseY > rectY && mouseY < y2)) {

 return true


}

}






function makeButtonVisual(rectX, rectY, rectSize, rectWidth, chosenColour, textString, prize) {
push();
noStroke();
//Shadow
fill(0, 0, 0, 0.01);
rect(rectX+5, rectY+5, rectWidth, rectSize, 10);
//Button
fill(0, 0, chosenColour);
rect(rectX, rectY, rectWidth, rectSize, 10);

//Text
fill(0, 0, 0);
textAlign(CENTER);
textFont('Helvetica');
textSize(20);
textStyle(BOLD);
text(textString, rectX + rectWidth/2, rectY + rectSize/2);

textSize(10);
text(prize, rectX + rectWidth/2, rectY + rectSize/2+15);




pop();
}


function makeAGram(){
push();
noStroke();
fill(0,0,100);

rect(100, 150, 351, 700, 35);


fill(0, 0, 0);
textAlign(CENTER);
textFont('Helvetica');
textSize(15);
textStyle(BOLD);

text('FOLLOWERS', 350, 250);
pop();
}

function createEmoji() {
push()
noStroke();
fill(260,100,50);

//head
ellipseMode(CENTER);
ellipse(180, 220, 100);

//eyes courtesy P5 curve reference
let point11 = { x: 130, y: 270};
let point12 = { x: 150, y: 210};
let point13 = { x: 170, y: 210};
let point14 = { x: 190, y: 270};

//distance between eyes
let eyeWidth = 40;

//drawing eyes as curves
stroke(0, 0, 0);
strokeWeight(5);
curve(point11.x, point11.y, point12.x, point12.y, point13.x, point13.y, point14.x, point14.y);
curve(point11.x + eyeWidth, point11.y, point12.x + eyeWidth, point12.y, point13.x + eyeWidth, point13.y, point14.x + eyeWidth, point14.y);

/*   I want to make the mouth curve in a way so the emoji
     is sad when there is a low amount of followers and happy
     when theres a high amount

     This requires some math to turn followers into points for the mouth curve
*/

//drawing the mouth

let happiness = 0;
let mouthPosition = 0;

// sad = y240 happy = y220
// Calculated in mathprogram maple so (10000 follower y = 0) and (0 followers = 360) using linear regression
happiness = (follower*-(9/250))+360;
mouthPosition = happiness*(1/18);

let point1 = { x: 110, y: happiness};
let point2 = { x: 140, y: 220 + mouthPosition};
let point3 = { x: 220, y: 220 + mouthPosition};
let point4 = { x: 250, y: happiness};

curve(point1.x, point1.y, point2.x, point2.y, point3.x, point3.y, point4.x, point4.y);



pop()

}





function draw() {

makeAGram();
//Knap1
trueClick(mouseX, mouseY, 600, 500);
makeButtonVisual(600, 500, 50, 100, chosenColour4, mansion, mansionPrize);

//Knap2
trueClick(mouseX, mouseY, 600, 400);
makeButtonVisual(600, 400, 50, 100, chosenColour3, yacht, yachtPrize);


//Knap3
trueClick(mouseX, mouseY, 600, 300);
makeButtonVisual(600, 300, 50, 100, chosenColour2, lambo, lamboPrize);


//Knap4
trueClick(mouseX, mouseY, 600, 200);
makeButtonVisual(600, 200, 50, 100, chosenColour1, rolex, rolexPrize);

// timer that activate at certain interval
if(frameCount % (interval * 30) == 0){
   follower += followspeed;
   }

//RolexButton
if (trueClick(mouseX, mouseY, 600, 200) == true && follower > 30) {
followspeed += 3;
follower -= 30;
chosenColour1 = 60;
showImg1 += 1;
}





//LamboButton
if (trueClick(mouseX, mouseY, 600, 300) == true && follower > 150) {
followspeed += 13;
follower -= 150;
chosenColour2 = 60;
showImg2 += 1;
}

//Yachtbutton
if (trueClick(mouseX, mouseY, 600, 400) == true && follower > 1000) {
followspeed += 15;
follower -= 1000;
chosenColour3 = 60;
showImg3 += 1;
}

//MansionButton
if (trueClick(mouseX, mouseY, 600, 500) == true && follower > 2000) {
followspeed += 50;
follower -= 2000;
chosenColour4 = 60;
showImg4 += 1;
}

textSize(40);
textAlign(CENTER);
text(follower, 350, 220);
console.log(followspeed);


createEmoji();

if (showImg1 >= 1) {
image(img1, 100, 320);
}

if (showImg2 >= 1) {
image(img2, 217, 320);
}

if (showImg3 >= 1) {
image(img3, 334, 320);
}

if (showImg4 >= 1) {
image(img4, 100, 437);
}


}

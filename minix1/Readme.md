![](minix1-billede.jpg)


**<h1>Gradient project</h1>**
___
<h5>Victor Rasmussen</h5>
<br>
<br>
<br>

I’ve produced something I call Gradient. Since I was around 12 years old I've used Photoshop to create all sorts of projects. In more recent years I've become very fond of gradients. So when i got the opportunity to program my first thoughts were of creating something with gradients. My idea was to use some kind of blur on random circles, which I know from photoshop experience, can create gradient-looking colormaps. My program essientially create 40 random circles in random colors which get blurred and makes this gradient effect. This means everytime time you run the code the gradients will be different.


 <h3>  colorMode() & HSL</h3>
<p>When you create gradients its important to make sure the colors match in some way, otherwise it will not look good. Therefore i made use of P5’s syntax <code>colorMode()</code> where i used the color system HSL. HSL works differently than RGB in the sense that the color is chosen from Hue, Saturation & Lightness. This makes it easier to make a system, where i can pick certain colors by having the same parameters for saturation and lightness, but changing the hue. This creates colors which fit well together for gradients.<p/>


 <h3> Bugs/Fixes </h3>
<p>In terms of bugs and fixes i encountered something interesting. I tried to use p5’s <code>filter(BLUR)</code> to blur the circles. While it did work the strain on the computer was very high. The <code>filter(BLUR)</code> syntax calculates blur from all pixels. This makes it so the server, i ran the test on, crashed several times when i tried to use it. I looked at different solutions and came across a youtube video by Kazuki Umeda explaining this phenomina(Umeda, 2022). Kazuki suggested calling a function within the Canvas API of javascript. This function essientially does the same in terms of the visual, but operates differently. This way the strain i less harder on the computer and server. The syntax looked like this: <code>drawingContext.filter = 'blur(175px)';</code> I also used the syntax of <code>push()</code> and <code>pop()</code> to keep the blur only relating to the circles, this syntax keeps the formatting of other syntax inside the start and end of <code>push()</code> and <code>pop()</code>. <p/>








 <h3> Trying to code </h3>
<p>This is not my first independent coding experience. But the first time I created something for anyone else to view. I think the P5’s reference site is so useful. As earlier mentioned I have experience from photoshop and when thinking about the possibilities of P5 i have a lot of ideas influenced by what i can do in Photoshop. Therefore when i have ideas the reference site, opens the door of possibilities of what i can make in P5. Coding is not constraining me in the same way. In P5 i can do a lot more, obviously the workflow is very different, but coding is in a way fulfilling in a different way. <p/>

<p>When thinking in code, it feels to me like compared to Photoshop the way of thinking is more logical-orientated. I dont have the visual tools of photoshop, but i have to think of what actually happens behind these tools. Sometimes it’s hard to get the syntax right and even thinking of where code is in relation to each other. I feel its very important to think about the order of the different syntax. When it comes to the execution. Also i had earlier problems with scope. Where i place different constants or variables have great meaning, because it needs to be somewhere i can pull the information into the functions. And also some functions only work in the Setup function.<p/>
 <h3> In relation to writing </h3>
<p>When i read or write text the order is very important in the same way as code. Obviously code isnt so forgiving as text. On a more abstract level i think all text relay information. When i read the information is for me, and when i code i write information for a computer to execute. <p/>


  <h3> Coding </h3>
<p>Programming is interesting as it opens a complete new world of possibilities. Coding is hard and it is not easy to go from idea to an executable code. But i think coding is such a universal tool. I can make generative art, a passion of mine, but at the same time coding can be very functional solving real world problems. Coding is so powerful and can change the world in many ways, both for the better and also for worse. As i am sorrounded by code every day i think it is important to be aware of what both code and i broader meanings of software mean to my lífe and also the society i live in. I think coding is a great way to get insight in this. <p/>


<h3> RUNME & REPOSITORY </h3>

[RUNME](https://vras.gitlab.io/aestheticprogramming/minix1/)
<br>
[My repository](https://gitlab.com/VRAS/aestheticprogramming)




<h3> Reference </h3>

[p5.js](https://p5js.org/)
<br>
Kazuki Umeda(2022) [How to get Fast Blur effect in p5.js](https://www.youtube.com/watch?v=s7CTmJt0NfI&t=18s)

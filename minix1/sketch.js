var colorNum;
var x;
var y;
var esize;
var hueColor;

//Number of different colors you want (can be changed)
colorNum = 12;

function setup() {
  createCanvas(800,800);
  frameRate(30);
//Explained in ReadMe about why i use HSL
  colorMode(HSL);
}

function drawEllipse() {
  //Random numbers for generation of circles. Both X & Y position, but also the size of the circle
  x = random(630);
  y = random(630);
  esize = random(500,700);

//Explained in ReadMe in more detail --- This is choosing a random HUE
  hueColor = floor(random(colorNum));

  noStroke();

  /*
  Calculation of HUE-steps
  Hue can take numbers between 0 & 360
  if i choose i would have 12 colours(colorNum) it can only pick 12 colours perfectly spaced between each other
  Example: If colour number is 4 it would be able to choose between: 90, 180, 270, 360 for hue value
  */
  fill(hueColor*360/colorNum,360,45);

  //drawing Ellipse
  ellipse(x, y, esize);
}

function draw() {

// draws ellipse for 40 frames and then prints text on screen (if/else statement)
  if (frameCount < 40) {
push()
  drawingContext.filter = 'blur(175px)';
  drawEllipse();

pop()
} else {
  textFont('Helvetica');
  textSize(100);
  textStyle(BOLD);
  fill(255);
  textAlign(CENTER);
  text('GRADIENT.', width/2, height/2);
  }
}

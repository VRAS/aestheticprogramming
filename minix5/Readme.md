![](00001.jpg)


**<h1>Generative Calendar</h1>**
___
**<h1>Please use my program before reading the readme!</h1>**
[<h2> RunMe </h2>](https://vras.gitlab.io/aestheticprogramming/minix5/)
<br>






In my program  I have a different set of rules. First of all i have a rule where noise determines how many rectangles are drawn, it also determines where the Y-position is for the rectangles every frame. Also I have randomly determined the length of the rectangles, with the use of random. In this way I have a contrast of the waves in the middle with the use of Perlin Noise, and the outer circle with the use of random, which look very different. Also noise determines the color.

I have also made a calendar system where noise determines the date from two parameters: day and month. The system isn’t perfect in terms of the days, because it has 31 days in each month, and therefore it could show dates which aren’t real.

The idea is the dates coupled with a specific setting on the noise pattern and therefore the date is represented with a unique circle.

My idea is to have a different kind of automatic progress where the program is going back and forth between progress and regress. It's an attempt to do something different with temporality, in terms of mixing progress with regress in a never ending manner.

I think my program isn’t exactly ideal, I'm somewhat disappointed in the result, and I don't think I have resolved this task in a good way. I will still call it generative art, as I think the program has its own protocol for generating, a lifelike, visual, but at the same time the liveliness could be clearer and better executed in other ways. That would require a completely new project. I had a hard time coming to an idea, which I thought would be good, and that resulted in having to choose one of my ideas and running with it, as I ran out of time towards the end of the week.

I would like to take some time to address this question from the week's reading.

“If the evolutionary neo-Darwinian logic of this — where the fittest survive — were not worrying enough, Game of Life is further troubling for its “necropolitical” dimension: articulating life and death in terms of populations and neighborhoods, as if part of a social cleansing program (or dystopian smart city project). Is this simply an example of poor abstraction?”

I think Game of Life is a good example of how computer coding or computer science is interesting. I mean for the take isn’t humanistic it is more a lifelike approach, where I see it as artificial life, in a non-human way. In this approach Game of Life is a beautyful visual, of an artificial organism living inside the screen.  On the other hand if the rules are based on a human approach, as it could be a representation of reality it can be problematic. It could then be seen as a simulation of truth or a simulation of realness, where parallels could be drawn to society in a damaging way, because computers act like there are told, and it is hard as a programmer to be able to code all of humanity in a non-affectionate language.







<h3> RUNME & REPOSITORY </h3>

[RUNME](https://vras.gitlab.io/aestheticprogramming/minix5/)
<br>
[My repository](https://gitlab.com/VRAS/aestheticprogramming)




<h3> Reference </h3>
<br>

Winnie Soon & Geoff Cox(2020) Aesthetic Programming: A Handbook of Software Studies.    [LINK HERE!]([p5.js](https://p5js.org/)



<br>

[p5.js](https://p5js.org/)

<br>

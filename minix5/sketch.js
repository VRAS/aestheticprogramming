let colour = 0;
let rectAmount;
let rectWidth = 1;
let yOffset = 30;
let minRectLength = 10;
let maxRectLength = 100;
let angle = 0;
let angleCalc = 0;
let numberCalc = 0;

let day = 0;
let month = 0;


monthText = ["Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.", "Jul.", "Aug.", "Sep", "Oct.", "Nov.", "Dec."];




function setup() {
 createCanvas(windowWidth, windowHeight);
 frameRate (10);
 angleMode(DEGREES)
 colorMode(HSL);
}

function draw() {

background(0,0,255);

numberCalc = map(noise(frameCount/10), 0, 1, 0, 360);

angleCalc = 360/numberCalc;

colour = map(noise(frameCount/30), 0, 1, 0, 360);

//calender-text
push();
textAlign(CENTER);
day = floor(map(noise(frameCount/100), 0, 1, 0, 30.99)) + 1;
month = floor(map(noise(frameCount/100), 0, 1, 0, 11.99));
fill(255,255,0);
textSize(20);
textStyle(BOLD);
text(monthText[month], windowWidth/2, windowHeight/2 + windowHeight/20);
pop();

push();
textAlign(CENTER);
textSize(32);
fill(255,255,0);
text(day, windowWidth/2, windowHeight/2);

pop();





noStroke();
angle++;
for (let i = 0; i < numberCalc ; i++) {
  push();
  translate(windowWidth / 2, windowHeight / 2);
  rotate(angleCalc*i);
  fill(colour, 80, 60);
  rect(0, floor(noise(i*frameCount/100)*yOffset) + height/10, rectWidth, random(minRectLength, maxRectLength));
  pop();
}




}

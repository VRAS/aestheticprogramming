
let data = [];
let total = 0;
let result = 0;
let frameStopCount = 100;




//preloading images
let img;
function preload() {
  dog = loadImage('dog.jpg');
  cat = loadImage('cat.jpg');
}



//setting canvassize. I'm beggining to it this way as the size of the canvas is a global variable and can be used to do positional calculations later
let Width = 800;
let Height = 800;



function setup() {
  createCanvas(Width, Height);
  frameRate(10);
  colorMode(HSL);
  textSize(50);

//drawing images
  image(dog, 400, 0);
  image(cat, 0, 0);
}


function getInfo() {
  //Only push data into "info"-array, when mouse is on the screen
 if ((mouseX > 0 && mouseX < Width) && frameCount < frameStopCount) {
  data.push(mouseX);
 }
}


//couldn't get javascript array-sum-syntax to work so I created my own. Once activated it will give me the sum of the data array.

function infoCalculation() {
for (let i = 0; i < data.length; i++) {
  total += data[i];
}
}


function draw() {
  getInfo();

// executing the function
  if(frameCount == frameStopCount) {
    infoCalculation();
  }

// calculate the average value on x value
  result = total / data.length;


//Screen message
  if(frameCount > frameStopCount + 10 && result > Width/2) {
    push();
    noStroke();
    textAlign(CENTER);
    fill(255)
    rect(Width/2-300, Height/2-50, 600, 75);
    fill(0);
    text('You are a dog-person', Width/2, Height/2);
    pop();
  }

  //Screen message
  if(frameCount > frameStopCount + 10 && result < Width/2) {
    push();
    noStroke();
    textAlign(CENTER);
    fill(255)
    rect(Width/2-300, Height/2-50, 600, 75);
    fill(0);
    text('You are a cat-person', Width/2, Height/2);
    pop();

  }

}

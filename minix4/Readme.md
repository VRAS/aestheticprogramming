![](0001.jpg)


**<h1>Who are you?</h1>**
___
**<h1>Please use my program before reading the readme!</h1>**
[<h2> RunMe </h2>](https://vras.gitlab.io/aestheticprogramming/minix4/)
<br>




So this time I thought of capturing data and what it really means to me. Obviously I think that data capturing is a serious threat to our freedom. Especially when data is captured without our knowledge. I especially draw on an experience watching “The great hack”, which is a Netflix documentary focusing on a firm called Cambridge Analytica. This firm is experienced in making marketing campaigns through the use of Facebook data. Their approach to data is though and through unethical. Their unethical use of this data had big societal disadvantages.
<br>
<br>

> “Data is the most valuable resource on earth,”

-- <cite>Brittany Kaiser, former business development director for Cambridge Analytica</cite>

<br>
<br>


I think this quote sums up my impression of data. In a world where so much activity is online data becomes a way of targeting individuals for all sorts of purposes, on a grand scale. I think the data market has too much freedom in terms of using this data. This data can be weaponized and used in malicious ways. It’s not also just how you use this data, but it’s also how you process this data. A lot of good examples have already been shown in a documentary called “Coded bias”. This shows the danger of algorithms processing data, and getting results which are unfair, or even dangerous for the integrity of society.



My program is an algorithm tracking the x-axis of the mouse, and from this information determines whether you are a cat or a dog person.


My project has taken a different approach than I initially thought it would. Obviously I'm critical to data capturing, and opposed to the idea that companies have information about which can be bought by companies with different interests. Therefore I initially thought I would do a project about the malicious use of data.

I think my project is a light, and even happy experience. I think a route of making something sinister or dark wasn't right for me, but in a way the algorithm I made only has one input, the x-axis of the mouse. The algorithm is so simple and is made with the presumption that users will mouse over their favorite animal, whether it would be cats or dogs. The algorithms doesn't account for anything beyond the mouse movement. This is my way of saying that there are major pitfalls for algorithms and the use of algorithms should be well considered, which isnt always the case in my opinion.

In terms of coding, I had trouble with finding syntax for making a sum of the numbers of an array. I had to make a <code>for-loop</code> which took all the data and summed it to one value. I chose to call this an algorithm because it determines something personal on the basis of data. i’ve tried to use the javascript syntax <code>numbers.add</code> which didn’t work. I’ve also had a bit of trouble determining the timing of the program. It was important with the timing of when the program would calculate and also for how long the program would capture data.





<h3> RUNME & REPOSITORY </h3>

[RUNME](https://vras.gitlab.io/aestheticprogramming/minix4/)
<br>
[My repository](https://gitlab.com/VRAS/aestheticprogramming)




<h3> Reference </h3>
<br>
Amer, K., & Noujaim, J. (2019). The Great Hack. Netflix.

Kantayya, S.(2020). Coded Bias. 



<br>

[p5.js](https://p5js.org/)

<br>

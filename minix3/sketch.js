

//used later for roundness animation on a square
let roundness = 0;
let roundnessSpeed;

// used later for colour animation
let colour = 0;
let colourSpeed;

// list of words the computer 'loads'. No real meaning to these word other than they are computer related
let firstWord = ['loading', 'processing', 'fetching', 'downloading', 'getting', 'managing', 'looking', 'playing'];
let secondWord = ["cat-image.png" , "stor-mand.mp3", "log-file324.exe", "ransomware", "DOOM", "Rocket League", "atom-live-server", "javascript", "p5.js", "Brøndby.danskemestre", "WannaCry.exe", "Trojan Horse", "i-love-you.word"];



//setting canvassize. I'm beggining to it this way as the size of the canvas is a global variable and can be used to do positional calculations later
let Width = 400;
let Height = 400;

//the angle which it is rotating
let angle = 1;


function setup() {
  createCanvas(Width, Height);
  frameRate(20);
  colorMode(HSL);
  textSize(20);
}


function draw() {

// goes up to fifty and then down to 0 --- repeatedly
if (roundness == 0) {
  roundnessSpeed = +1;
} if (roundness == 50) {
  roundnessSpeed = -1;
}

//same as before with different number
if (colour == 0) {
  colourSpeed = +5;
} if (colour == 255) {
  colourSpeed = -5;
}

// used to make rotation/roundness/colour happen as an animation
angle = angle + 0.1;

  roundness += roundnessSpeed;
  colour += colourSpeed;


//setting up background in a place where its drawn behind my text and throbber
  background(200,50,95);


// Ubuntu server simulation --- made fast so it seems overwhelming in terms of processing powers
push()
fill(255,255,255,10);
  for (let i = 0; i < 20; i++) {

    text(firstWord[floor(random(firstWord.length))], 10, 20*i );
    text(secondWord[floor(random(secondWord.length))], 130, 20*i );
  }
pop()

// throbber is drawn. Note: roundness & colour are 'animated' numbers.
push();
translate(Width / 2, Height / 2);
rotate(angle);

// "rectmode(CENTER)" makes maths easier in terms of getting the object to spin around itself
rectMode(CENTER);
fill(colour, 75, 40);
noStroke();
rect(0,0,100,100,roundness);
pop();
}

![](001.jpg)


**<h1>Throbber</h1>**
___
<h5>Victor Rasmussen</h5>
<br>
<br>
<br>

Before i'm going to explain what I initially wanted to explore, i want to take a step back and look at what a throbber is to me.

A throbber is for me an icon/animation that is a placeholder for waiting associated with machine work happening in the background. To get closer to this idea its typically one of two feelings. First it has to do with the power of the computer. Does it have to load something which requires a lot of processing power.  An example could be a 3d model, this derives a lot of processing power especially from the GPU. Here will an icon appear indicating it has to get some time to load in the model to a visual medium. Here the icon suggests the computer has to have time in order to do calculations in order to render or in other terms translate the file to this program. I also feel like the fact the icon always is an animation is to suggest work or processing is in progress. If the icon isn't spinning it would be hard to know if the computer has crashed or in other ways isn't functioning and thereby working. The animation is key for me to understand it is in the process of loading. Obviously this is important and therefore translates into my work.
Secondly I feel a throbber can be associated with a company. An example could be on a browser where loading occurs on websites. I typically feel this has to do with the company’s ressources in terms of servers. If a website is loading a lot I associate this with lack of proffesionalism and I also get frustrated. In this scenario a throbber gets personal in another way. For me it is the company responsible for this loading.

I would like to highlight this quote from Winnie Soon’s Throbber: Executing Micro-temporal Streams

>*”Users usually encounter a distinctive spinning icon during the loading, waiting and streaming of data content. From a user perspective, this spinning icon represents an unstable streaming of the now. A graphical animation known as throbber indicates to users that something is loading and in-progress, but nothing more. A similar yet very different form of process indicator, such as a progress bar, expresses even more information than a throbber. In contrast to a progress bar, which is more linear in form, a throbber does not indicate any completed or finished status and progress. It does not explain any processual tasks in any specific detail”*
(Soon, Winnie 2019)

I would like to highlight this as a main reason for frustration. The fact that you don’t get information about a process other than it is in-progress leads to an expectation that it must be a short while, otherwise you would have a progress bar relating to more intricate and demanding processes. I would like to add that a throbber on websites is in a way more personal. Obviously the encounter with throbbers is for the most part related to processes initiated by yourself. Hereby throbbers are personal for you as a user, therefore I feel like frustration is between you as a consumer and the company in charge of the website.

Personally i had a lot of problems with a firm called Discovery and their streaming service DPLAY, which is now called Discovery+. The firm had bad servers or streaming technology, lesser than what you would expect from a mastodont of sports television in Europe. A throbber appeared every time it couldnt load in the stream fast enough and this throbber became my nemesis. The throbber became a symbol of the service not working to the expected standard. This led to some bad reviews on trustpilot and also i couldn't stand the throbber icon in the end.


What i think is interesting about a throbber is especially two things. First I think it's interesting that a throbber in some way is unwanted. A throbber by itself is not unwanted as it conveys some information, but the processes behind and thereby the waiting is unwanted. It could be useful to make a throbber which evolves or visually stimulates the user, in an attempt to make this time seem shorter or slightly more meaningful.

Secondly I think a throbber is interesting as it is a visual symbol of technological processes. How is a spinning icon a symbol that conveys that a computer is working, in the background. I think it’s kind of interesting relating this to an experience i had with an Ubuntu server. On a Ubuntu server it has an interface with the absolute bare minimum. It has text-input, and prints similarly to javascript’s “console.log” the processes happening in the background. Interestingly for me this kind of feedback is more satisfying as it seems like the computer is working a lot faster, when in reality the speed is the same as when a throbber is used.


This time around I made a simpler project. Using less code, and less different syntax. I was interested in morphing geometry. Something where the state of morph between two different stages are appearant. I was thinking how I would be able to have something change between a square and a circle in a seamless way. I thought of the <code>roundness()</code> syntax. This syntax can round a square by all sides, I thought to myself if and can fully round all sides it must be a square. Sure enough it worked and I could do an animation where it changes states between. For me the morphing represents the state between loading, where servers or computers do work in between two states. I couldn’t use <code>for()</code> loops as i had to have this animation to happen constantly and never stop. It is a series of code i need to be ran every frame in the program. Therefore i used an never ending loop, which is draw. I used a for loop, to make the program simulate ubuntu servers feedback. I needed to have 20 sentences printed on screen and therefore i could use a for loop instead of writing the <code>text()</code> function 20 times.



<h3> RUNME & REPOSITORY </h3>

[RUNME](https://vras.gitlab.io/aestheticprogramming/minix3/)
<br>
[My repository](https://gitlab.com/VRAS/aestheticprogramming)




<h3> Reference </h3>
<br>
Soon, Winnie (2019) Throbber: Executing Micro-temporal Streams, Computational Culture

[Avalible here](http://computationalculture.net/throbber-executing-micro-temporal-streams/)

<br>

[p5.js](https://p5js.org/)

<br>

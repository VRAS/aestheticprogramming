![](0000001.jpg)


**<h1>What is now a simple platformer</h1>**
___
**Press SPACE to jump**

In this miniX I've decided to stick a little closer to the questions than usual.


**Which MiniX have you reworked?**

So in this miniX i’ve reworked my miniX6. I wasn’t satisfied with the result of my miniX6. This is a quote from my readme for my minix6: “So I did not finish my game. My plan was to make a very simple platformer where you would have to jump over some amount of space, of course when you didn’t, you would then lose, and the objective would be to see how far you could get. I thought to myself a platformer with these very basic functions shouldn’t be so difficult to do. But I was so wrong.”

In this quote I lay out the basics of the program. My program didn’t at the time feature the jumping from the character and it also needed some detection that recognized when the player isn’t able to jump over the gaps between the platforms.


**What have you changed and why?**

Mentioned above my game did lack some key features in order to be a playable game. What a game is a difficult question to answer entirely, but in this case, I think that in order to be a game, I would need two things: Player input, you would have to control the jump of the player. And also game rules. There would have to be rules in place determining when you would lose the game.

My task was initially 3 things. I would like to fix jumping, and game rules. I would also like to add a system which created a score. So you would have an incentive to perform well in the game. Also I thought it would be a great way to make the game playable and comparable between players so they would be able to compete against each other.

I would like to outline how my objects are different now.

| LevelGenerator |
| ------------- |
| this.mover, this.speed, this.yposition, this.platform, this.platform1, this.jumplength & this.hitboxarray |   
| move(), show()    |   

| Jumping Character |
| ------------- |
| this.size,this.characterx, this.yposition, this.direction, this.velocity, this.jumpstrength, this.fallingspeed,  this.groundlevel & this.jump |   
| Display() jumpingphysics() jumptrigger() gravity() |   


**What have you learnt in this miniX? How did you incorporate/advance/interpret the concepts in your ReadMe/RunMe (the relation to the assigned readings)?**


I’ve learned how to use classes in p5, and also how it relates to object-oriented programming. How fundamentally different the approach is in terms of the working process. When you dont program in an object-oriented way, you don’t need as much planning. When creating objects, you have to have an idea of how your program is going to work. Because your classes need to function in a certain way and are reliant on other parts of your program in order to work. In classes you are able to edit methods which is important, but it can still be problematic because the classes rely on a certain setup in order to work. It’s an entirely different way to program and you have to take it into consideration while planning. On the other hand, I find it very beneficial when you want more objects, with the same methods, in this way it creates a piece of code which is very logical, and more precise, than doing it without classes, because it’s meant for this purpose.

I think the key in my concept is object abstraction.
>“Object abstraction in computing is about representation. Certain attributes and relations are abstracted from the real world, whilst simultaneously leaving details and contexts out. Let’s imagine a person as an object (rather than a subject) and consider which properties and behaviors that person might have. ”

When creating my concept for a game I have to think about how I want to extract different things from the real world into my program. The abstraction happens in the translation from the real world into code. I need to decide how the code abstracts the real world. My game has taken a lot of inspiration from the game-genre platformer. Platformers is an established genre and a lot of these platformer games abstract from the real-world differently. My inspiration is especially from Nintendo’s Super Mario series, where the world often is 2D world, viewed from the side and where the main mechanic is jump. But I also diverted thematically from the game with a very basic geometric style, and my game doesn't have any story to it. I think my object abstraction is not only an abstraction from the real world, but also an abstraction from other games I've played. When creating a concept I, as a designer, draw a lot on my experience playing games.

 I think my game reflects my ‘gaming-competitiveness’ because the game hasn’t any story, it has a score, which is the main incentive to play the game. The game is hopefully joyful to play, but the idea is you would play against friends for the highest score.



What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?

>“To reiterate the point, our intention is for readers to acquire key programming skills in order to read, write and think with, and through, code (and we will return to the issue of literacy later). We feel that it is important to further explore the intersections of technical and conceptual aspects of code in order to reflect deeply on the pervasiveness of computational culture and its social and political effects”

I think it’s interesting pairing this quote with my approach to coding. In a way when I code I always start with something that is close to me. Which typically is an experience. My approach then broadens and the ideation of the program can begin. I try as much as I can to be aware that whatever I'm working on, is not just a showcase of what I can do in terms of coding, and using the right syntax. But for me Aesthetic programming broadens what I previously had known as coding. Coding previously was writing code, and thinking of syntax and making code become a program. Coding is now completely different, in a way you are the author of a program, which is accessible for other people, this means you have a responsibility for what you do. Ethics has to be a core part of your work, and not only ethics, it’s about what you can do for the world, how you want to set a mark for the world. It’s about the inclusion of all people. You can criticize existing  consensus and change something for the better. This introduction I feel is necessary for the following points I’m going to make.

I think my program is a reflection on my own experiences playing computer. My game does not directly criticize a certain point of view, but it has everything unnecessary taken away. I think the approach is interesting for a couple of reasons. 1. What is a game?: in a way this is my reflection for this miniX. How do I approach the task of making a game? What is even a game to me? I think in a way this project is a personal odé to what gaming is for me. I also think it's interesting how game thematics is thought out, and I think it’s interesting how a game is when it’s stripped from any real-world or typical fantasy themes, and how the visuals only consist of rectangles.




<h3> RUNME & REPOSITORY </h3>

[RUNME](https://vras.gitlab.io/aestheticprogramming/minix7/)
<br>
[My repository](https://gitlab.com/VRAS/aestheticprogramming)




<h3> Reference </h3>
<br>

Winnie Soon & Geoff Cox(2020) Aesthetic Programming: A Handbook of Software Studies.    [LINK HERE!]([p5.js](https://p5js.org/)


<br>

[p5.js](https://p5js.org/)

<br>

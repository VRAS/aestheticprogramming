let levelgenerator = [];
let player = [];

//one point 4 times a second
//let highscore = floor(frameCount/15);


function setup() {
  createCanvas(400, 400);
  frameRate(60)
}

function draw() {
  background(0);
  starterBox();
  Start();
  showLevelGenerator();
  checkBoundary();
  createNew();

  playerStart();
  playerIterator();

  hitboxDetection();

text(floor(frameCount/15), 370, 30);

//console.log(player[0].yposition);



}

//box to start
function starterBox(){
  push();
  fill(255);
    if (frameCount <= 80) {
      rect(0,300,400 - (frameCount * 5), 100);
  pop();
  }
}
//showing the boxes
function showLevelGenerator(){
  for (let i = 0; i <levelgenerator.length; i++) {
    levelgenerator[i].move();
    levelgenerator[i].show();
  }
}

//splice when boxes are all the way through the screen
  function checkBoundary(){

  for (let i = 0; i < levelgenerator.length; i++) {

    if ((levelgenerator[i].platform1 + levelgenerator[i].jumplength) + levelgenerator[i].mover  <= 0) {
      levelgenerator.splice(i,1);
    }
  }
}

//creates new when end of boxes hit the right side of screen
function createNew(){

  for (let i = 0; i < levelgenerator.length; i++) {

    if (((levelgenerator[i].platform1 + levelgenerator[i].jumplength) + levelgenerator[i].mover  <= 410) && levelgenerator.length == 1) {
      levelgenerator.push(new LevelGenerator());
    }
  }
}
//starting to spawn boxes
    function Start(){
if (levelgenerator.length <= 0) {
      levelgenerator.push(new LevelGenerator());
    }
  }
//player
function playerStart(){
if (player.length <= 0) {
      player.push(new Jumpingcharacter());
    }
  }
//player methods
function playerIterator() {
  for (let i = 0; i <player.length; i++) {
    player[i].display();
    player[i].jumptrigger();
    player[i].jumpphysics();
    player[i].gravity();
  }
}
//hitbox detection checks the if player hit the jump area
  function hitboxDetection() {

  for (let i = 0; i < levelgenerator.length; i++) {

    if (((width/2 - 10 >= levelgenerator[i].hitboxarray[0] + levelgenerator[i].mover) && (width/2 - 10 <= levelgenerator[i].hitboxarray[1] + levelgenerator[i].mover)) && player[0].yposition >= 250) {
//What happens when you lose (scoreboard & noLoop)
push();
rectMode(CENTER);
fill(255);
rect(width/2, height/2, 150);
fill(0);
textAlign(CENTER);
text('your score is:', width/2, height/2 - 10);
textSize(20);
text(floor(frameCount/15), width/2, height/2 + 30);
pop();
      noLoop();
    }
  }
}

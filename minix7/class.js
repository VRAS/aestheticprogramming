class LevelGenerator {
    constructor()
    {
      //initalizing the objects
      this.mover = 400;

      //slowly incrementing speed so it will make the harder and harder as time goes.
      this.speed = 5;

      //Area before jump
      this.yposition = height-100;

      this.platform = 0;
      this.platform1 = random(300,600);

      //Creating random jumplengths
      this.jumplength = this.platform1 + random(75,150);

      this.hitboxarray = [this.platform1, this.jumplength];


    }

   move() {
    //moves the level across the screen
     this.mover -= this.speed;
   }


  show() {
    push();
    noStroke();
    fill(255);
    translate(this.mover, this.yposition);


    rect(this.platform, this.platform, this.platform1, 100);
    rect(this.jumplength, this.platform, this.platform1, 100);

    pop();
  }
}

class Jumpingcharacter {
    constructor()
    {
      this.size = 50
      this.characterx = width/2 - 10;
      this.jump = 0;

      this.yposition = height - 100 - this.size;

      this.jump;

     //Manipulating gravity
     //these numbers control the gravity
     //A lot of inspiration from youtube creator "Mr. Erdreich"

     //these variebles also allow to change how high you jump, and how quickly you would fall down.
     //Essientially being a difficulty regulator
     this.direction = 1;
     this.velocity = 10;
     this.jumpstrength = 75;
     this.fallingspeed = 2;
     this.groundlevel = 250;

    }
jumptrigger(){
  if((keyCode == 32 && keyIsPressed) && this.yposition >= this.groundlevel) {
  this.jump = true;
  }
  else {
  this.jump = false;
}

}
gravity(){
if(this.yposition >= this.groundlevel && this.jump == false)  {
  this.yposition = this.yposition;
}
else{
  this.yposition = this.yposition + (this.direction*this.velocity); // Downwards gravity
 }
}

jumpphysics(){
  if(this.jump == true) {
  this.velocity = -this.jumpstrength;
 }
else {
 this.velocity = this.fallingspeed;
 }
}

  display(){
    noStroke();
    fill(255,120,120);
    rect(this.characterx, this.yposition + this.jump, 20, this.size);
  }
}

class LevelGenerator {
    constructor()
    {
      //initalizing the objects
      this.mover = 0;

      //slowly incrementing speed so it will make the harder and harder as time goes.
      this.speed = 5;

      //Area before jump
      this.yposition = height-100;

      this.platform = 0;
      this.platform1 = random(300,600);

      //Creating random jumplengths
      this.jumplength = this.platform1 + random(75,150);


    }

   move() {
    //moves the level across the screen
     this.mover -= this.speed + (frameCount/10*0.001);
   }


  show() {
    push();
    noStroke();
    fill(220);
    translate(this.mover, this.yposition);


    rect(this.platform, this.platform, this.platform1, 100);
    rect(this.jumplength, this.platform, this.platform1, 100);

    pop();
  }

}

class Jumpingcharacter {
    constructor()
    {
      this.size = 50
      this.characterx = width/2 - 10;
      this.jump = 0;

      this.yposition = height - 100 - this.size;

    }






  display(){
    noStroke();
    fill(255,120,120);
    rect(this.characterx, this.yposition + this.jump, 20, this.size);
  }
}

![](000001.jpg)


**<h1>What should've been a simple platformer</h1>**
___
**<h1>Please use my program before reading the readme!</h1>**
[<h2> RunMe </h2>](https://vras.gitlab.io/aestheticprogramming/minix6/)
<br>


So I did not finish my game. My plan was to make a very simple platformer where you would have to jump over some amount of space, of course when you didn’t, you would then lose, and the objective would be to see how far you could get. I thought to myself a platformer with these very basic functions shouldn’t be so difficult to do. But i was so wrong.

Implementing animation in terms of jump, and making a system that would be able to check if the jump was successful was a big task for me, especially in relation to the assignment of one week. I think I was way too ambitious this time, in terms of my actual skills.

The state the program is in now I have two objects. This first is

| LevelGenerator |
| ------------- |
| this.mover, this.speed, this.yposition, this.platform, this.platform1 & this.jumplength    |   
| move(), show()    |   





Second is Jumping Character


| Jumpingcharacter |
| ------------- |
| this.size,this.characterx, this.yposition   |   
| display()    |


The program right now only features a static character and automatic level generation. I’m pretty satisfied with the level generation, because it is able to spawn and despawn different paths in the code relating to how much it has moved.

Programming in an object-oriented style has proven difficult for me. I had a hard time learning how to use the syntax in praxis. I encountered numerous errors regarding the syntax, and in general it was a tedious job, but I think I got the basics down now, but I still limited myself, with the ideation of a project I wasn't able to realize.

The world generator is working within an array. When the game starts it spawns the first level of ground with 2 platforms separated by a jump. it will the wait until the upper left corner of the rear platform reaches the vector (400,100) it will then spawn a new set, by pushing the object in the array, and again wait and spawn again. It will keep doing this throughout the program. It will despawn when the upper left corner as earlier mentioned reaches the vector (0,100). In does the by splicing the object from the array.


>“When tofu becomes a computational object — as in Tofu Go! — abstraction is required to capture the complexity of processes, and relations, and to represent what are thought to be essential or desirable properties, and behaviors. In the game, tofu is designed as a simple, three-dimensional white cube with a range of emotive expressions, and the ability to move, and jump. Of course real tofu cannot behave this way, but you can imagine how objects perform if you have programmed your own game,...”

When you program it is impossible to recreate the real world, in a way you would try to emulate ‘a real world’. Sometimes it is desirable to move away from real-world properties and create properties which can be enjoyable in a game. As in Tofu Go! by Francis Lam. I also think the emulation can be a bit misleading, in a way a jump is easy for us humans to do, but if you have to program a jump, you’ll have to emulate forces pulling in the character or mass, which is very complicated to do. Even to do an unrealistic jump, you would have to create your own set of forces. I think in some way it can be misleading, to think programming real world or modified real-world behavior is easily done. Which is a lesson I learned the hard way this time. I think abstraction is something you have to train, in order to compare the feasibility with your skills. Abstraction is a great way to move away from real-world properties to create a new set of rules so to speak, in order to make games or movies which are enjoyable. For example my favorite game is Rocket League, which essentially has an unrealistic set of physics, but the physics in the game makes for a great playing experience. If the game was realistic it wouldn’t be fun. I can also imagine abstraction being a problem. In a way abstraction in game design is an artform. I think abstraction and the interpretations in designing a game is what makes a game great. But i can see the abstraction also being a problem in terms of some individuals making choices which for them mimic a real-world scenario, but in reality could be very exclusionary, or have an interpretation lying under which could be problematic.








<h3> RUNME & REPOSITORY </h3>

[RUNME](https://vras.gitlab.io/aestheticprogramming/minix6/)
<br>
[My repository](https://gitlab.com/VRAS/aestheticprogramming)




<h3> Reference </h3>
<br>

Winnie Soon & Geoff Cox(2020) Aesthetic Programming: A Handbook of Software Studies.    [LINK HERE!]([p5.js](https://p5js.org/)

Francis Lam - Tofu Go! [LINK HERE!](https://www.dbdbking.com/Tofu-Go) 

<br>

[p5.js](https://p5js.org/)

<br>
